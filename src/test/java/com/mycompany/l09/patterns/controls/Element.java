package com.mycompany.l09.patterns.controls;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

class Element {

    WebElement webElement;
    WebDriverWait wait;

    Element(WebDriver driver, WebElement webElement) {

        this.webElement = webElement;
        wait = new WebDriverWait(driver, 10);
    }

}
