package com.mycompany.l09.patterns.controls;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class Button extends Element {

    public Button(WebDriver driver, WebElement webElement) {
        super(driver, webElement);
    }

    public void click() {
        wait.until(visibilityOf(webElement)).click();
    }

}
