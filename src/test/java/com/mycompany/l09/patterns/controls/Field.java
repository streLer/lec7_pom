package com.mycompany.l09.patterns.controls;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class Field extends Element {

    public Field(WebDriver driver, WebElement webElement) {
        super(driver, webElement);
    }

    public Field enterText(String text) {
        wait.until(visibilityOf(webElement)).clear();
        wait.until(visibilityOf(webElement)).sendKeys(text);
        return this;
    }
}
