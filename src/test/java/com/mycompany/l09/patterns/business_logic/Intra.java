package com.mycompany.l09.patterns.business_logic;

import com.mycompany.l09.patterns.app.Application;
import com.mycompany.l09.patterns.page_objects.LoginPage;
import com.mycompany.l09.patterns.page_objects.UserMainPage;
import org.openqa.selenium.WebDriver;

public class Intra {

    private final Application app;

    public Intra(Application app) {
        this.app = app;
    }

    public MyProfile login(String userName, String password) {
        WebDriver driver = app.getWebDriver();
        driver.get("https://intra.t-systems.ru/login");

        UserMainPage userMainPage = new LoginPage(driver)
                .enterUsername(userName)
                .enterPassword(password)
                .clickLogin();
        return new MyProfile(userMainPage);
    }
}
