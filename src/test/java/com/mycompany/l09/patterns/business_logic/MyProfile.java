package com.mycompany.l09.patterns.business_logic;

import com.mycompany.l09.patterns.page_objects.UserMainPage;

public class MyProfile {

    private final UserMainPage userMainPage;

    MyProfile(UserMainPage userMainPage) {
        this.userMainPage = userMainPage;
    }

    public Groups openGroups() {
        return new Groups(userMainPage.clickGroups());
    }

}
