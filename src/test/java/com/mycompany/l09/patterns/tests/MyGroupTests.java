package com.mycompany.l09.patterns.tests;

import com.mycompany.l09.patterns.business_logic.Intra;
import com.mycompany.l09.patterns.domain.Group;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class MyGroupTests extends TestBase {

    private Intra intra;

    @BeforeMethod
    public void initData() {
        intra = app.getIntra();
    }

    static Loader loader = new Loader();

    public static final String USERNAME = loader.getLogin();
    public static final String PASSWORD = loader.getPassword();

    public static String e2ePrefix = "E2E-";
    public static String randomGroupName = RandomStringUtils.randomAlphanumeric(10).toUpperCase();

    @Test
    public void createGroup() {
        var group = new Group()
                .withName(e2ePrefix + randomGroupName)
                .withDescription("Description for a test group 5 , april 19 04 2021")
                .open(true);

        var groupDetails = intra.login(USERNAME,PASSWORD)
                .openGroups()
                .createGroup(group);

        groupDetails.assertThat()
                .groupNameIs(group.getName());
    }

}
