package com.mycompany.l09.patterns.page_objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class GroupDetailsPage extends BasePage {

    @FindBy(id = "UserName")
    private WebElement usernameField;

    @FindBy(xpath = "//a[.//span[contains(text(), 'Управление')]]")
    private WebElement managementTab;

    @FindBy(xpath = "//input[contains(@placeholder, 'Название')]")
    private WebElement groupNameField;

    @FindBy(xpath = "//input[@type = 'radio' and @value='1']")
    private WebElement closedRadioButton;

    @FindBy(xpath = "//div[@class='InfoBlock-Group-DataName']")
    private WebElement groupNameLabel;

    @FindBy(css = "[data-bind *= groupVisibility]")
    private WebElement groupVisibility;

    @FindBy(xpath = "//a[contains(text(), 'Сохранить')]")
    private WebElement saveButton;

    GroupDetailsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public GroupDetailsPage clickManagementTab() {
        wait.until(elementToBeClickable(managementTab)).click();
        return this;
    }

    public GroupDetailsPage enterGroupName(String newGroupName) {
        wait.until(visibilityOf(groupNameField)).clear();
        groupNameField.sendKeys(newGroupName);
        return this;
    }

    public GroupDetailsPage clickClosedRadioButton() {
        wait.until(elementToBeClickable(closedRadioButton)).click();
        return this;
    }

    public String getGroupName() {
        return wait.until(visibilityOf(groupNameLabel)).getText();
    }

    public String getOpeness() {
        return wait.until(visibilityOf(groupVisibility)).getText();
    }

    public void clickSave() {
        wait.until(elementToBeClickable(saveButton)).click();
        sleep(1_000);
    }
}
