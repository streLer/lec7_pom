package com.mycompany.l09.patterns.page_objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class TstorePage extends BasePage {

    @FindBy(css = ".Shop-Header-Search-CurrencyValue")
    private WebElement currencyValue;

    TstorePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public TstorePage assertTstoreIsOpened() {
        wait.until(ExpectedConditions.visibilityOf(currencyValue));
        Assert.assertTrue(currencyValue.isDisplayed());
        return new TstorePage(driver);
    }
}
