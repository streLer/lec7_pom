package com.mycompany.l09.patterns.page_objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Page objects for ajax:
 * http://internetka.in.ua/ajaxelementlocatorfactory/
 *
 * FieldDecorator:
 * https://habr.com/ru/post/134462/
 */
class BasePage {

    WebDriver driver;
    WebDriverWait wait;

    BasePage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
