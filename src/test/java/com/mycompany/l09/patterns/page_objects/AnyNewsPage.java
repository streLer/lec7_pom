package com.mycompany.l09.patterns.page_objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class AnyNewsPage extends BasePage {

    @FindBy(xpath = "//div[@class='PryanikyEditorV1']")
    private WebElement textBlock;

    @FindBy(css = ".Icon_comment-lines")
    private WebElement commentIcon;

    AnyNewsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public AnyNewsPage assertNewsIsOpened() {
        wait.until(ExpectedConditions.visibilityOf(textBlock));
        Assert.assertTrue(commentIcon.isDisplayed());
        return new AnyNewsPage(driver);
    }
}
