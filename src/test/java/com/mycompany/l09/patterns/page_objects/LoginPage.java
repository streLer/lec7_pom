package com.mycompany.l09.patterns.page_objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class LoginPage extends BasePage {

    @FindBy(id = "LOGINUSERNAME")
    private WebElement usernameField;

    @FindBy(id = "LOGINPASSWORD")
    private WebElement passwordField;

    @FindBy(id = "LOGINENTERBUTTON")
    private WebElement loginButton;

    @FindBy(className = "Login-ErrorMessage")
    private WebElement errorMessageArea;

    public LoginPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public boolean isPageOpen() {
        return usernameField.isDisplayed();
    }

    public LoginPage enterUsername(String username) {
        wait.until(visibilityOf(usernameField)).clear();
        usernameField.sendKeys(username);
        return this;
    }

    public LoginPage enterPassword(String username) {
        wait.until(visibilityOf(passwordField)).clear();
        passwordField.sendKeys(username);
        return this;
    }

    public UserMainPage clickLogin() {
        wait.until(elementToBeClickable(loginButton)).click();
        return new UserMainPage(driver);
    }
}
