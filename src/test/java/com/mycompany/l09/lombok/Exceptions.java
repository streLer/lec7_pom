package com.mycompany.l09.lombok;

import lombok.SneakyThrows;

class Exceptions {

    //@SneakyThrows
    void doSmth() throws NoSuchMethodException {
        System.out.println(Exceptions.class.getMethod("doSmth").getName());
    }

}
