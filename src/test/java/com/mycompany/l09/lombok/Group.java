package com.mycompany.l09.lombok;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Group {

    private String name;

    private String description;

    private boolean isOpen;

    public String getName() {
        System.out.println("getName() is invoked");
        return name;
    }

}
